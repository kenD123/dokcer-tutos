#build phase
FROM node:alpine as builder
WORKDIR '/app3'
COPY package.json .
RUN npm install
COPY . .
RUN npm run build

#run phase
FROM nginx
COPY --from=builder /app3/build /usr/share/nginx/html



